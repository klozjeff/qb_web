'use strict';

angular.module('app')
  .controller('FooterCtrl', function ($scope, $state, Utils) {
    $scope.goTo = function(url){
        $state.go(url); 
      }
   
  });
