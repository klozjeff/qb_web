'use strict';

angular.module('app')
  .controller('NavbarCtrl', function ($scope, $state) {
    $scope.goTo = function(url){
        $state.go(url); 
      }
   
  });
