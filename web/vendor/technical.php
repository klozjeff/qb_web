<?php
session_set_cookie_params(172800);
session_start();
define('IS_HOME','true');
require('app/core/config/config.php');
require('app/core/config/config-theme.php');
require('app/core/config/config-lang.php');
require('app/core/system.php');
//echo $theme;

$db->set_charset('utf8');

if(isset($_GET['mobileapp'])) {
   $_SESSION['mobileapp'] = true;
} 

if(isset($_SESSION['mobileapp'])) {
   $mobileapp = true;
} else {
   $mobileapp = false;
}
include('scripts/create_order.php');
?>
<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie ie9" lang="en-US">
<![endif]-->
<html lang="en-US">

<!-- Mirrored from torchtemplates.net/educa/single-course.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Jan 2016 12:22:25 GMT -->
<head>
      <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<title><?php echo $site_name; ?>-A Unique Essay Writing Service. Get Professional Academic Help</title>

<?php include('templates/header-links.php'); ?>
<script type="text/javascript">
    var TypeOfPaper = '{"typeOfPaper":{"HighSchool":[{"option":"Essay (any type)","ratio":"1.00"},{"option":"Admission essay","ratio":"1.00"},{"option":"Annotated bibliography","ratio":"1.00"},{"option":"Argumentative essays","ratio":"1.00"},{"option":"Article review","ratio":"1.00"},{"option":"Biographies","ratio":"1.00"},{"option":"Book\/movie review","ratio":"1.00"},{"option":"Business plan","ratio":"1.00"},{"option":"Capstone project","ratio":"1.00"},{"option":"Case study","ratio":"1.00"},{"option":"Coursework","ratio":"1.00"},{"option":"Creative writing","ratio":"1.00"},{"option":"Critical thinking","ratio":"1.00"},{"option":"Editing","ratio":"0.60"},{"option":"Formatting","ratio":"0.30"},{"option":"Other","ratio":"1.00"},{"option":"Presentation or speech","ratio":"1.00"},{"option":"Proofreading","ratio":"0.50"},{"option":"Research paper","ratio":"1.00"},{"option":"Research proposal","ratio":"1.00"},{"option":"Term paper","ratio":"1.00"}],"College":[{"option":"Essay (any type)","ratio":"1.00"},{"option":"Admission essay","ratio":"1.00"},{"option":"Annotated bibliography","ratio":"1.00"},{"option":"Argumentative essays","ratio":"1.00"},{"option":"Article review","ratio":"1.00"},{"option":"Biographies","ratio":"1.00"},{"option":"Book\/movie review","ratio":"1.00"},{"option":"Business plan","ratio":"1.00"},{"option":"Capstone project","ratio":"1.00"},{"option":"Case study","ratio":"1.00"},{"option":"Coursework","ratio":"1.00"},{"option":"Creative writing","ratio":"1.00"},{"option":"Critical thinking","ratio":"1.00"},{"option":"Editing","ratio":"0.60"},{"option":"Formatting","ratio":"0.30"},{"option":"Other","ratio":"1.00"},{"option":"Presentation or speech","ratio":"1.00"},{"option":"Proofreading","ratio":"0.50"},{"option":"Research paper","ratio":"1.00"},{"option":"Research proposal","ratio":"1.00"},{"option":"Term paper","ratio":"1.00"}],"University":[{"option":"Essay (any type)","ratio":"1.00"},{"option":"Admission essay","ratio":"1.00"},{"option":"Annotated bibliography","ratio":"1.00"},{"option":"Argumentative essays","ratio":"1.00"},{"option":"Article review","ratio":"1.00"},{"option":"Biographies","ratio":"1.00"},{"option":"Book\/movie review","ratio":"1.00"},{"option":"Business plan","ratio":"1.00"},{"option":"Capstone project","ratio":"1.00"},{"option":"Case study","ratio":"1.00"},{"option":"Coursework","ratio":"1.00"},{"option":"Creative writing","ratio":"1.00"},{"option":"Critical thinking","ratio":"1.00"},{"option":"Dissertation","ratio":"1.00"},{"option":"Dissertation chapter - Conclusion","ratio":"1.00"},{"option":"Dissertation chapter - Discussion","ratio":"1.00"},{"option":"Dissertation chapter - Introduction","ratio":"1.00"},{"option":"Dissertation chapter - Methodology","ratio":"1.00"},{"option":"Dissertation chapter - Results","ratio":"1.00"},{"option":"Dissertation chapter - Review","ratio":"1.00"},{"option":"Editing","ratio":"0.60"},{"option":"Formatting","ratio":"0.30"},{"option":"Other","ratio":"1.00"},{"option":"Presentation or speech","ratio":"1.00"},{"option":"Proofreading","ratio":"0.50"},{"option":"Research paper","ratio":"1.00"},{"option":"Research proposal","ratio":"1.00"},{"option":"Term paper","ratio":"1.00"},{"option":"Thesis","ratio":"1.00"},{"option":"Thesis\/Dissertation abstract","ratio":"1.00"},{"option":"Thesis\/Dissertation chapter","ratio":"1.00"},{"option":"Thesis\/Dissertation proposal","ratio":"1.00"}],"Masters":[{"option":"Essay (any type)","ratio":"1.00"},{"option":"Admission essay","ratio":"1.00"},{"option":"Annotated bibliography","ratio":"1.00"},{"option":"Argumentative essays","ratio":"1.00"},{"option":"Article review","ratio":"1.00"},{"option":"Biographies","ratio":"1.00"},{"option":"Book\/movie review","ratio":"1.00"},{"option":"Business plan","ratio":"1.00"},{"option":"Capstone project","ratio":"1.00"},{"option":"Case study","ratio":"1.00"},{"option":"Coursework","ratio":"1.00"},{"option":"Creative writing","ratio":"1.00"},{"option":"Critical thinking","ratio":"1.00"},{"option":"Dissertation","ratio":"1.00"},{"option":"Dissertation chapter - Conclusion","ratio":"1.00"},{"option":"Dissertation chapter - Discussion","ratio":"1.00"},{"option":"Dissertation chapter - Introduction","ratio":"1.00"},{"option":"Dissertation chapter - Methodology","ratio":"1.00"},{"option":"Dissertation chapter - Results","ratio":"1.00"},{"option":"Dissertation chapter - Review","ratio":"1.00"},{"option":"Editing","ratio":"0.60"},{"option":"Formatting","ratio":"0.30"},{"option":"Other","ratio":"1.00"},{"option":"Presentation or speech","ratio":"1.00"},{"option":"Proofreading","ratio":"0.50"},{"option":"Research paper","ratio":"1.00"},{"option":"Research proposal","ratio":"1.00"},{"option":"Term paper","ratio":"1.00"},{"option":"Thesis","ratio":"1.00"},{"option":"Thesis\/Dissertation abstract","ratio":"1.00"},{"option":"Thesis\/Dissertation chapter","ratio":"1.00"},{"option":"Thesis\/Dissertation proposal","ratio":"1.00"}],"PhD":[{"option":"Essay (any type)","ratio":"1.00"},{"option":"Admission essay","ratio":"1.00"},{"option":"Annotated bibliography","ratio":"1.00"},{"option":"Argumentative essays","ratio":"1.00"},{"option":"Article review","ratio":"1.00"},{"option":"Biographies","ratio":"1.00"},{"option":"Book\/movie review","ratio":"1.00"},{"option":"Business plan","ratio":"1.00"},{"option":"Capstone project","ratio":"1.00"},{"option":"Case study","ratio":"1.00"},{"option":"Coursework","ratio":"1.00"},{"option":"Creative writing","ratio":"1.00"},{"option":"Critical thinking","ratio":"1.00"},{"option":"Dissertation","ratio":"1.00"},{"option":"Dissertation chapter - Conclusion","ratio":"1.00"},{"option":"Dissertation chapter - Discussion","ratio":"1.00"},{"option":"Dissertation chapter - Introduction","ratio":"1.00"},{"option":"Dissertation chapter - Methodology","ratio":"1.00"},{"option":"Dissertation chapter - Results","ratio":"1.00"},{"option":"Dissertation chapter - Review","ratio":"1.00"},{"option":"Editing","ratio":"0.60"},{"option":"Formatting","ratio":"0.30"},{"option":"Other","ratio":"1.00"},{"option":"Presentation or speech","ratio":"1.00"},{"option":"Proofreading","ratio":"0.50"},{"option":"Research paper","ratio":"1.00"},{"option":"Research proposal","ratio":"1.00"},{"option":"Term paper","ratio":"1.00"},{"option":"Thesis","ratio":"1.00"},{"option":"Thesis\/Dissertation abstract","ratio":"1.00"},{"option":"Thesis\/Dissertation chapter","ratio":"1.00"},{"option":"Thesis\/Dissertation proposal","ratio":"1.00"}]}}';
   var AcademyPriceDependenceList = '{"High School":{"1209600":"7.00","604800":"7.00","432000":"9.00","259200":"9.00","172800":"12.00","86400":"15.00"},"College":{"1209600":"10.00","604800":"10.00","432000":"13.00","259200":"13.00","172800":"17.00","86400":"21.00"},"University":{"1209600":"12.00","604800":"12.00","432000":"15.00","259200":"15.00","172800":"21.00","86400":"25.00"},"Masters":{"1209600":"16.00","604800":"16.00","432000":"21.00","259200":"21.00","172800":"25.00","86400":"30.00"},"PhD":{"1209600":"19.00","604800":"21.00","432000":"25.00","259200":"27.00","172800":"30.00","86400":"35.00"}}';
        </script>
</head>
<body>

	
	<div class="sidebar-menu-container" id="sidebar-menu-container">

		<div class="sidebar-menu-push">

			<div class="sidebar-menu-overlay"></div>

			<div class="sidebar-menu-inner">
	<?php include('templates/header.php'); ?>
				

			



				<section class="single-course">
					<div class="container page-container">
						<div class="row">
							<div class="col-md-8">
								<div class="single-item">		
									<div class="row">		
										<div class="col-md-12">
											<div class="description">
												<h1 class="page-h1" style="text-align:left">How it Works</h1>
													</div>
											<div class="section-heading">
											<h1>Most scholars have mis-sold their path to academic excellence.With these simple steps we academically assist them to ensure a satisfying career goals achievement:</h1>
											<img src="assets/images/line-dec.png" alt="">
										
										</div>
											<div class="topics welcome-intro-2 welcome-to-educa" style="margin-left:10px">
												
												<div class="row">
													<div class="service-item section-heading">
											<i style="font-weight:bold">#1</i>
											<h1>Place your Order</h1>
											<div class="line-dec"></div>
											<span style="color:#000">Simply fill order form with your papers details for our professional writers to have clear guidelines of your requirements.</span>
											<div style="margin-left:30px"><p>✓ Its Simple & Easy </p>
											<p>✓ Fill as much information as you can </p>
											<p>✓ Helps us get your order requirement right for quality paper </p>
											</div>
										</div>
										
											<div class="service-item section-heading">
											<i style="font-weight:bold">#2</i>
											<h1>Reserve Funds</h1>
											<div class="line-dec"></div>
											<span style="color:#000">You dont need to pay upfront to get our academic assistance services.Just Order and Reserve your funds.Only release payments after you get satisfied with quality of work provided.</span>
										<div style="margin-left:30px"><p>✓ 100% Money Back Guarantee </p>
											<p>✓ Release Payments as per your order completion </p>
											
											</div>
										</div>
										
										<div class="service-item section-heading">
											<i style="font-weight:bold">#3</i>
											<h1>Track Order Progress</h1>
											<div class="line-dec"></div>
											<span style="color:#000">Watch as your paper being handled by our professional writers step by step(From Requistion to Completion)</span>
										<div style="margin-left:30px"><p>✓ Helps in minimal Revision </p>
											<p>✓ Enhance quality delivery within stipulation time frame </p>
												<p>✓ This is an ACADEMIC ASSISTANCE SERVICE-Hold discussion on paper content in real time from our enhanced writer-customer chat platform </p>
											</div>
										</div>
										<div class="service-item section-heading">
											<i style="font-weight:bold">#4</i>
											<h1>HURRAY!!You are now certified A+ Graduate</h1>
											<div class="line-dec"></div>
											<!--<span style="color:#000">Watch as your paper is being handled by our professional writers step by step(From Requistion to Completion).Just Order and Reserve(OR) your funds.Only pay after you get what you requested as per terms and condition in relation to your order.</span>
										-->
										<div style="margin-left:30px"><p>✓ Hardwork+ EssayDuck Assistance & Collaboration=Guaranteed Success </p>
										
											</div>
										</div>
													
												</div>
											</div>
											
										</div>
									</div>
								</div>
							</div>
                            <div class="col-md-4">
																<?php include('templates/form-widget.php');?>
																	<?php include('templates/more-disciplines.php');?>
								
                            				                          </div>
                            
						</div>
					</div>
				</section>
				<?php include('templates/footer.php');?>
		<script src="assets/js/jquery.customSelect.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.touchSwipe.min.js" type="text/javascript"></script>
<script src="assets/js/script.js" type="text/javascript"></script></body>
</html>