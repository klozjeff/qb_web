<?php
session_set_cookie_params(172800);
session_start();
require('core/config/config.php');
require('core/config/config-theme.php');
require('core/config/config-lang.php');
require('core/system.php');

$page['name'] = $lang['Orders'];
$menu['home'] = 'active';

if(isset($_GET['filter'])) {
if(isset($_GET['sort_by'])) {
$sort_by = $_GET['sort_by'];
switch ($sort_by) {
   case 1:
     $sort_by = 'ORDER BY id DESC';
     break;
   case 2:
     $sort_by = 'ORDER BY id ASC';
     break;
   case 3:
     $sort_by = 'ORDER BY last_active DESC';
     break;
   default:
     $sort_by = 'ORDER BY RAND()';
     break;
 } 
}
if(isset($_GET['page_range'])) {
$page_range = $_GET['page_range'];
$page_range = explode(',',$page_range);
$min_page = $page_range[0];
$max_page = $page_range[1];
}
} else {
$sort_by = 'ORDER BY RAND()';
$min_page = 1;
$max_page = 100;
}

if($user['isAdmin'] == 1)
{
   $feeds = $db->query("SELECT qb_feeds.* FROM qb_feeds WHERE sender_id='".$user['companyWorking']."'");
}
/*if($user['is_admin'] == 2)
{
$orders=$db->query("SELECT order_details.* FROM order_details WHERE customer_id=".$user['id']." AND (order_pages>= $min_page AND order_pages<= $max_page) $sort_by");
//echo "SELECT * FROM order_details WHERE customer_id =".$user['id']." AND (order_page >= $min_page AND order_page <= $max_page) $sort_by";
}
if($user['is_admin'] ==0)
{
	$orders=$db->query("SELECT order_details.* FROM order_details WHERE (writer_preference=".$user['id']." OR writer_preference='') AND (order_pages>= $min_page AND order_pages<= $max_page) $sort_by");

}*/


if($user['updated_preferences'] == 0) {
  $page['js'] .= '
  <script>
  swal({ 
    title: "'.$lang['Welcome_Window_Title'].'",
    text: "'.$lang['Welcome_Window_Text'].'",
    imageUrl: "img/salute.png",
    confirmButtonText: "'.$lang['OK'].'",
    confirmButtonColor: "#3a3f51",
    closeOnConfirm: false,
    closeOnCancel: false
  },
  function(){
    window.location.href = "preferences.php";
  });
</script>
';
} else {
  $page['js'] .= '
  <script>
  function clickHeart(id) {
    heart = $("#heart-"+id);
    $.get("ajax/clickHeart.php?id="+id, function(data) {
      $("#user-"+id).html(data);
    });
    heart.css("color", "#f05050");
    heart.animo( { animation: ["tada"], duration: 2 } );
  }
  </script>
';
}

if(isset($_POST['delete_conversation'])) {
$delete = $_POST['delete'];
foreach($delete as $k=>$v) {
$db->query("UPDATE order_details SET status='Archive' WHERE id='".$v."'");
}
header('Location: '.$domain.'/app/chats');
exit;
}
if(isset($_POST['custom']))
{
$custom	= $_POST['custom'];
$custom = explode('/', $custom);
$energy = $custom[0];
$user = $custom[1];


$db->query("UPDATE users SET energy=energy+".$energy." WHERE id='".$user."'");
}

$users = $db->query("SELECT id FROM qb_users WHERE companyFollowing='".$user['companyWorking']."'")->num_rows;
$purchases = $db->query("SELECT * FROM qb_feeds WHERE sender_id='".$user['companyWorking']."'")->num_rows;
$chats = $db->query("SELECT * FROM qb_tickets WHERE receiver_id='".$user['companyWorking']."'")->num_rows;
require('inc/top.php');
?>
<section>
<div class="content-wrapper">
<!--<h3>
<?php echo $lang['Orders']?> 
<span class="pull-right"> 
<a class="btn btn-primary" data-toggle="modal" data-target="#filterResults"> <i class="fa fa-sliders fa-fw"></i> <?php echo $lang['Filter']?> </a> 
<?php if(isset($_GET['filter'])) { ?>
<a href="orders?filter=true&sort_by=<?php echo $_GET['sort_by']?>&page_range=<?php echo $_GET['page_range']?>" class="btn btn-default"> <i class="fa fa-refresh fa-fw"></i> Refresh </a> 
<?php } else { ?>
<a href="orders" class="btn btn-default"> <i class="fa fa-refresh fa-fw"></i> <?php echo $lang['Refresh']?> </a> 
<?php } ?>
</span> 
</h3>-->
<div class="container-fluid">

<div class="row">

<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-purple">
<div class="row row-table">
<div class="col-xs-4 text-center bg-purple-dark pv-lg">
<em class="icon-credit-card fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $purchases?></div>
<div class="text-uppercase">Feeds</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-primary">
<div class="row row-table">
<div class="col-xs-4 text-center bg-primary-dark pv-lg">
<em class="icon-user fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $users?></div>
<div class="text-uppercase">Loggers</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-12">
<div class="panel widget bg-green">
<div class="row row-table">
<div class="col-xs-4 text-center bg-green-dark pv-lg">
<em class="icon-bubbles fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $chats?></div>
<div class="text-uppercase">Chats</div>
</div>
</div>
</div>
</div>

<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-purple">
<div class="row row-table">
<div class="col-xs-4 text-center bg-purple-dark pv-lg">
<em class="icon-credit-card fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0">3546</div>
<div class="text-uppercase">Payshop</div>
</div>
</div>
</div>
</div>
</div>
<div class="table-grid table-grid-desktop">
<div class="col">
<div class="clearfix mb">
<div class="btn-group pull-left">
</div>
</div>
<?php if($feeds->num_rows >= 1) { ?>
<form action="" method="post">
<div class="row">
<div class="col-md-5">
<!--<button type="submit" name="new" class="btn btn-default"> <i class="fa fa-pencil fa-fw"></i> New Message </button>-->
<button type="submit" name="delete_conversation" class="btn btn-danger"> <i class="fa fa-trash fa-fw"></i> Archive Feeds </button>
<a href="new-feed"><button type="" name="" class="btn btn-success"> <i class="fa fa-plus fa-fw"></i> New Feed </button></a>
</div>
</div>
<br>
<div class="panel panel-default">
<div class="panel-body">
	<table class="table table-responsive mb-mails">
	   <thead>
	  
	   <th style="text-align:left;">F.No</th>
                    <th style="text-align:left;">Content</th>
					  <th style="text-align:left;"> Posted </th>
                    <th style="text-align:left;"> Actions </th>
                    <th style="text-align:center;">  </th>
                </thead>
		<tbody>
			<?php 
			$t=0;
			while($details = $feeds->fetch_array()) {
				$t+=1;
			?>
				<tr>
				
				<td>
	                <a href="order/<?php echo $details['id']?>" style="text-decoration:none;">
							
							
							
							<div class="mb-mail-meta">
								<div class="pull-left">
									<div class="mb-mail-preview"><?php echo $t?></div>
								</div>
								
							</div>

						</a>
             	</td>
					
					<td>
						<a href="order/<?php echo $details['id']?>" style="text-decoration:none;">
							
						
							<div class="mb-mail-meta">
								<!--<div class="pull-left">
									<div class="mb-mail-subject" style="color:#515253;"><?php echo $details['content']?></div>
								</div>-->
								<div class="mb-mail-preview"><?php echo $details['content']?></div>
							
							
							</div>

						</a>
					</td>
					<td><?php echo time_ago($details['time'])?></td>
						<td></td>
					
					<td style="width:40px;text-align:center;">
	                <div class="checkbox c-checkbox">
	                   <label>
	                      <input name="delete[]" type="checkbox" value="<?php echo $details['id']?>">
	                      <span class="fa fa-check"></span>
	                   </label>
	                </div>
             	</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
</form>
<?php } else { echo 'No Feeds to Show'; } ?>
</div>
</div>
</div>
</div>
</section>
<?php
require('inc/bottom.php'); 
?>