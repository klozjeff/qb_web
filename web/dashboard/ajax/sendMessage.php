<?php
session_start();
define('IS_AJAX','true');
require('../core/config/config.php');
require('../core/config/config-lang.php');
require('../core/system.php');

$user1 = $user['companyWorking'];
$user2 = $db->real_escape_string($_GET['id']);
$message = $db->real_escape_string($_GET['msg']);

$check = $db->query("SELECT id FROM qb_tickets WHERE (receiver_id='".$user1."' AND sender_id='".$user2."') OR (sender_id='".$user2."' AND receiver_id='".$user1."')");
if($check->num_rows == 1) {
  $convers = $check->fetch_array();
  $db->query("INSERT INTO qb_ticketmessages (ticket_id,message,sender_id,time) VALUES ('".$convers['id']."','".$message."','".$user1."','".time()."')");
  $convers_id = $convers['id'];

} else {
  $db->query("INSERT INTO qb_tickets (sender_id,receiver_id,time) VALUES ('".$user1."','".$user2."','".time()."')");
  $convers_id = $db->insert_id;
  $db->query("INSERT INTO qb_ticketmessages (convers_id,message,sender_id,time) VALUES ('".$convers_id."','".$message."','".$user1."','".time()."')");
}

$db->query("UPDATE qb_tickets SET last_activity='".time()."',last_message='".$message."' WHERE id='".$convers_id."'");

//$db->query("UPDATE users SET last_active='".time()."' WHERE id='".$user['id']."'");
$messages = $db->query("SELECT * FROM qb_ticketmessages WHERE ticket_id='".$convers_id."' AND (sender_id = '".$user1."' OR sender_id='".$user2."') ORDER BY id DESC");
$db->query("INSERT INTO qb_feeds (receiver_id,sender_id,url,content,icon,time) VALUES ('$user2','$user1','web/chat/".$user1."','".sprintf($lang['messaged_you'],$user['full_name'])."','fa fa-comment', '".time()."')");
/*$receiver_email = $db->query("SELECT email FROM users WHERE id='".$user2."'")->fetch_array();
$receiver_email = $receiver_email['email'];
$clean = str_replace('<b> ', '', $lang['messaged_you']);
$clean = str_replace(' </b>', '', $clean);
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
mail($receiver_email, $site_name.' - '.sprintf($clean,$user['full_name']), '<a href="'.$domain.'/app/chat/'.$user1.'">'.sprintf($lang['messaged_you'],$user['full_name']).'</a>',$headers,'-f info@'.$domain);
*/
while($message = $messages->fetch_array()) {
//$sender = $db->query("SELECT id,profile_picture,full_name FROM users WHERE id='".$message['sender_id']."'")->fetch_array();
if($message['sender_id']== $user['companyWorking']) { $sender = $db->query("SELECT qb_company.companyName as full_name,qb_company.companyID AS id FROM qb_company id='".$message['sender_id']."'")->fetch_array(); } 
if($message['sender_id']!= $user['companyWorking']) { $sender = $db->query("SELECT qb_users.id AS id,qb_users.unique_id AS full_name FROM qb_users WHERE id='".$message['sender_id']."'")->fetch_array(); } 

?>
<!-- START list group item-->
<a href="#" class="list-group-item">
<div class="media-box">
<div class="pull-left">
<img src="<?php echo getProfilePicture($domain,$sender)?>" class="media-box-object img-rounded thumb32">
</div>
<div class="media-box-body clearfix">
<small class="pull-right"><?php echo time_ago($message['time'])?></small>
<strong class="media-box-heading text-primary">
<?php echo $sender['full_name']?></strong>
<p class="mb-sm">
<small><?php echo parseEmoticons($domain,$message['message'])?></small>
</p>
</div>
</div>
</a>
<!-- END list group item-->
<?php	
}
