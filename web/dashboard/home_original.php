<?php
session_set_cookie_params(172800);
session_start();
require('core/config/config.php');
require('core/config/config-theme.php');
require('core/config/config-lang.php');
require('core/system.php');

$page['name'] = $lang['Orders'];
$menu['home'] = 'active';

if(isset($_GET['filter'])) {
if(isset($_GET['sort_by'])) {
$sort_by = $_GET['sort_by'];
switch ($sort_by) {
   case 1:
     $sort_by = 'ORDER BY id DESC';
     break;
   case 2:
     $sort_by = 'ORDER BY id ASC';
     break;
   case 3:
     $sort_by = 'ORDER BY last_active DESC';
     break;
   default:
     $sort_by = 'ORDER BY RAND()';
     break;
 } 
}
if(isset($_GET['page_range'])) {
$page_range = $_GET['page_range'];
$page_range = explode(',',$page_range);
$min_page = $page_range[0];
$max_page = $page_range[1];
}
} else {
$sort_by = 'ORDER BY RAND()';
$min_page = 1;
$max_page = 100;
}

if($user['is_admin'] == 1)
{
   $orders = $db->query("SELECT order_details.* FROM order_details WHERE id!='' AND (order_pages >= $min_page AND order_pages <= $max_page) $sort_by");
}
if($user['is_admin'] == 2)
{
$orders=$db->query("SELECT order_details.* FROM order_details WHERE customer_id=".$user['id']." AND (order_pages>= $min_page AND order_pages<= $max_page) $sort_by");
//echo "SELECT * FROM order_details WHERE customer_id =".$user['id']." AND (order_page >= $min_page AND order_page <= $max_page) $sort_by";
}
if($user['is_admin'] ==0)
{
	$orders=$db->query("SELECT order_details.* FROM order_details WHERE (writer_preference=".$user['id']." OR writer_preference='') AND (order_pages>= $min_page AND order_pages<= $max_page) $sort_by");

}


if($user['updated_preferences'] == 0) {
  $page['js'] .= '
  <script>
  swal({ 
    title: "'.$lang['Welcome_Window_Title'].'",
    text: "'.$lang['Welcome_Window_Text'].'",
    imageUrl: "img/salute.png",
    confirmButtonText: "'.$lang['OK'].'",
    confirmButtonColor: "#3a3f51",
    closeOnConfirm: false,
    closeOnCancel: false
  },
  function(){
    window.location.href = "preferences.php";
  });
</script>
';
} else {
  $page['js'] .= '
  <script>
  function clickHeart(id) {
    heart = $("#heart-"+id);
    $.get("ajax/clickHeart.php?id="+id, function(data) {
      $("#user-"+id).html(data);
    });
    heart.css("color", "#f05050");
    heart.animo( { animation: ["tada"], duration: 2 } );
  }
  </script>
';
}

if(isset($_POST['delete_conversation'])) {
$delete = $_POST['delete'];
foreach($delete as $k=>$v) {
$db->query("UPDATE order_details SET status='Archive' WHERE id='".$v."'");
}
header('Location: '.$domain.'/app/chats');
exit;
}
if(isset($_POST['custom']))
{
$custom	= $_POST['custom'];
$custom = explode('/', $custom);
$energy = $custom[0];
$user = $custom[1];


$db->query("UPDATE users SET energy=energy+".$energy." WHERE id='".$user."'");
}

require('inc/top.php');
?>
<section>
<div class="content-wrapper">
<h3>
<?php echo $lang['Orders']?> 
<span class="pull-right"> 
<a class="btn btn-primary" data-toggle="modal" data-target="#filterResults"> <i class="fa fa-sliders fa-fw"></i> <?php echo $lang['Filter']?> </a> 
<?php if(isset($_GET['filter'])) { ?>
<a href="orders?filter=true&sort_by=<?php echo $_GET['sort_by']?>&page_range=<?php echo $_GET['page_range']?>" class="btn btn-default"> <i class="fa fa-refresh fa-fw"></i> Refresh </a> 
<?php } else { ?>
<a href="orders" class="btn btn-default"> <i class="fa fa-refresh fa-fw"></i> <?php echo $lang['Refresh']?> </a> 
<?php } ?>
</span> 
</h3>
<div class="container-fluid">
<div class="table-grid table-grid-desktop">
<div class="col">
<div class="clearfix mb">
<div class="btn-group pull-left">
</div>
</div>
<?php if($orders->num_rows >= 1) { ?>
<form action="" method="post">
<div class="row">
<div class="col-md-5">
<!--<button type="submit" name="new" class="btn btn-default"> <i class="fa fa-pencil fa-fw"></i> New Message </button>-->
<button type="submit" name="delete_conversation" class="btn btn-danger"> <i class="fa fa-trash fa-fw"></i> Archive </button>
<a href="<?php echo $domain.'/order';?>"><button type="" name="" class="btn btn-success"> <i class="fa fa-plus fa-fw"></i> New Order </button></a>
</div>
</div>
<br>
<div class="panel panel-default">
<div class="panel-body">
	<table class="table table-responsive mb-mails">
	   <thead>
	   <th style="text-align:center;">  </th>
	   <th style="text-align:left;"> ID</th>
                    <th style="text-align:left;"> Topic </th>
					 <th style="text-align:left;"> <?php echo $lang['Price']?> </th>
					  <th style="text-align:left;"> Paid </th>
                    <th style="text-align:left;"> Status </th>
					  <th style="text-align:left;">	<div class="pull-right">Posted</div> Deadline </th>
                   
                </thead>
		<tbody>
			<?php while($details = $orders->fetch_array()) {
				//if($convers['user1'] != $user['id']) { $other_user = $db->query("SELECT id,full_name,profile_picture FROM users WHERE id='".$convers['user1']."'")->fetch_array(); } 
				//if($convers['user2'] != $user['id']) { $other_user = $db->query("SELECT id,full_name,profile_picture FROM users WHERE id='".$convers['user2']."'")->fetch_array(); } 
				$price=$db->query("SELECT * FROM order_price WHERE order_price.order_id='".$details['id']."'")->fetch_array();
				?>
				<tr>
				<td style="width:40px;text-align:center;">
	                <div class="checkbox c-checkbox">
	                   <label>
	                      <input name="delete[]" type="checkbox" value="<?php echo $details['id']?>">
	                      <span class="fa fa-check"></span>
	                   </label>
	                </div>
             	</td>
				<td >
	                <a href="order/<?php echo $details['id']?>" style="text-decoration:none;">
							
							
							
							<div class="mb-mail-meta">
								<div class="pull-left">
									<div class="mb-mail-preview">#<?php echo $details['unique_no']?></div>
								</div>
								
							</div>

						</a>
             	</td>
					
					<td>
						<a href="order/<?php echo $details['id']?>" style="text-decoration:none;">
							
						
							<div class="mb-mail-meta">
								<div class="pull-left">
									<div class="mb-mail-subject" style="color:#515253;"><?php echo $details['order_topic']?></div>
								</div>
								<div class="mb-mail-preview"><?php echo $details['order_type']?></div>
							
							
							</div>

						</a>
					</td>
					<td><?php echo	'$'.$price['total_cost']?></td>
						<td><?php echo	'$'.$price['paid']?></td>
					<td>
						<a href="order/<?php echo $details['id']?>" style="text-decoration:none;">
					
							<div class="mb-mail-meta">
							
								<div class="mb-mail-preview"><?php echo $details['status']?></div>
							
							
							</div>

						</a>
					</td>
					<td>
						<a href="order/<?php echo $details['id']?>" style="text-decoration:none;">
							
							<div class="mb-mail-date pull-right"><?php echo time_ago($details['time'])?></div>
							<div class="pull-left">
							<div class="mb-mail-meta">
								
								<div class="mb-mail-preview"><?php echo time_ago($details['order_deadline'])?></div>
							
							
							</div>
							</div>

						</a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
</form>
<?php } else { echo $lang['No_Orders_To_Show']; } ?>
</div>
</div>
</div>
</div>
</section>
<?php
require('inc/bottom.php'); 
?>