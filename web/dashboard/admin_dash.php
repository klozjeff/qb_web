<?php
$users = $db->query("SELECT id FROM qb_users WHERE companyFollowing='".$user['companyWorking']."'")->num_rows;
$purchases = $db->query("SELECT * FROM qb_feeds WHERE sender_id='".$user['companyWorking']."'")->num_rows;
$chats = $db->query("SELECT * FROM qb_tickets WHERE receiver_id='".$user['companyWorking']."'")->num_rows;
?>
<div class="row dashboardWidget">
<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-purple widget1">
<div class="row row-table">
<div class="col-xs-4 text-center pv-lg">
<em class="icon-feed fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $purchases?></div>
<div class="text-uppercase">Feeds</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-primary widget2">
<div class="row row-table">
<div class="col-xs-4 text-center pv-lg">
<em class="icon-user fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $users?></div>
<div class="text-uppercase">Loggers</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-12">
<div class="panel widget bg-green widget3">
<div class="row row-table">
<div class="col-xs-4 text-center pv-lg">
<em class="icon-bubbles fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $chats?></div>
<div class="text-uppercase">Chats</div>
</div>
</div>
</div>
</div>

<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-purple widget4">
<div class="row row-table">
<div class="col-xs-4 text-center pv-lg">
<em class="icon-credit-card fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0">3546</div>
<div class="text-uppercase">Payshop</div>
</div>
</div>
</div>
</div>
</div>