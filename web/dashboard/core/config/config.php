<?php
$domain = 'http://localhost/queryback/web';

// Database Configuration
$_db['host'] = 'localhost';
$_db['user'] = 'root';
$_db['pass'] = '';
$_db['name'] = 'queryback';

$site_name = 'QueryBack.com';
$meta['keywords'] = 'Academic Assistance Services,Writing help, Paper writing service, Get one page for free, Get $10 for free,Write my essay, Essay writing service, Custom writing paper, Custom essay,term papers, writing term papers, term paper writing, term paper service, term paper writing service, custom term paper writing, paper writing service, custom papers, term paper writing company';
$meta['description'] = 'A reliable  Academic assistance services. We offer low prices, high-quality papers, an educated staff and a chance to choose an appropriate writer.';

// PayPal Configuration
$paypal_email = 'essayduck@gmail.com'; // Email of PayPal Account to receive money

// PayGol Configuration (SMS Payments)
$paygol_service_id = '';

// Stripe Configuration
$secret_key = ''; // Stripe API Secret Key
$publishable_key = ''; // Stripe API Publishable Key

// Facebook Login Configuration
$fb_app_id = ''; 
$fb_secret_key = ''; 

$db = new mysqli($_db['host'], $_db['user'], $_db['pass'], $_db['name']) or die('MySQL Error');
	