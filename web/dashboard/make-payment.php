
<?php
session_start();
//define('IS_ADMIN','true');
require('core/config/config.php');
require('core/config/config-theme.php');
require('core/config/config-ads.php');
require('core/config/config-lang.php');
require('core/system.php');

if(!isLogged() || $user['isAdmin'] == 0) { header('Location: ../index.html'); exit; }

$page['name'] = 'Admin - Global Notification';
$menu['home'] = 'active';

if(isset($_POST['send'])) {
$content = $_POST['content'];
$users = $db->query("SELECT id FROM qb_users");
while($_user = $users->fetch_array()) {
$id = $_user['id'];
$db->query("INSERT INTO qb_feeds(receiver_id,sender_id,url,content,icon,time,is_read) VALUES ('".$id."','".$user['companyWorking']."','#','".$content."','fa fa-globe', '".time()."', '0')");
$success = true;
}
}

$users = $db->query("SELECT id FROM qb_users WHERE companyFollowing='".$user['companyWorking']."'")->num_rows;
$purchases = $db->query("SELECT * FROM qb_feeds WHERE sender_id='".$user['companyWorking']."'")->num_rows;
$chats = $db->query("SELECT * FROM qb_tickets WHERE receiver_id='".$user['companyWorking']."'")->num_rows;

require('inc/top.php');
?>
<section>
<div class="content-wrapper">
<div class="container-fluid">
<div class="row">

<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-purple">
<div class="row row-table">
<div class="col-xs-4 text-center bg-purple-dark pv-lg">
<em class="icon-credit-card fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $purchases?></div>
<div class="text-uppercase">Feeds</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-primary">
<div class="row row-table">
<div class="col-xs-4 text-center bg-primary-dark pv-lg">
<em class="icon-user fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $users?></div>
<div class="text-uppercase">Loggers</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-12">
<div class="panel widget bg-green">
<div class="row row-table">
<div class="col-xs-4 text-center bg-green-dark pv-lg">
<em class="icon-bubbles fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $chats?></div>
<div class="text-uppercase">Chats</div>
</div>
</div>
</div>
</div>

<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-purple">
<div class="row row-table">
<div class="col-xs-4 text-center bg-purple-dark pv-lg">
<em class="icon-credit-card fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0">3546</div>
<div class="text-uppercase">Payshop</div>
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<form action="" method="post">
<div class="panel panel-default">
<div class="panel-heading panel-title"> New Feed </div>
<div class="panel-body">
<?php if(isset($success)) { ?> <div class="alert alert-success"> <i class="fa fa-check fa-fw"></i> Notification has been sent </div> <?php } ?>
From here you can send a custom feed to all users <br><br>
<textarea class="form-control" name="content" placeholder="Feed content..."></textarea>
<br>
<input type="submit" name="send" class="btn btn-danger" value="Send">
</div>
</form>
</div>
</div>
</div>
</div>
</section>
<script src="<?php echo $domain?>/vendor/modernizr/modernizr.js"></script>
<script src="<?php echo $domain?>/vendor/jquery/dist/jquery.js"></script>
<script src="<?php echo $domain?>/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="<?php echo $domain?>/vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
<script src="<?php echo $domain?>/vendor/animo.js/animo.js"></script>
<script src="<?php echo $domain?>/vendor/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo $domain?>/app/js/app.js"></script>
</body>
</html>