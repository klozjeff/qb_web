<?php
session_set_cookie_params(172800);
session_start();
require('core/config/config.php');
require('core/config/config-theme.php');
require('core/config/config-lang.php');
require('core/system.php');

$page['name'] = $lang['Settings'];
$menu['my-friends'] = 'active';

$page['js'] .= '<script src="'.$domain.'/vendor/parsleyjs/dist/parsley.min.js"></script>';
$page['js'] .= '<script type="text/javascript">$("#settings").parsley();</script>';
$page['js'] .= '<script src="'.$domain.'/vendor/bootstrap-filestyle/src/bootstrap-filestyle.js"></script>';

if($user['updated_name'] == 0) {
	$can_update_name = '';
} else {
	$can_update_name = 'disabled';
}

if($user['private_profile'] == 0) {
	$private_profile = '';
} else {
	$private_profile = 'checked';
}

if(isset($_POST['save'])) {

	$email=$_POST['email'];
	$full_name=$_POST['full_name'];
	$password = trim($_POST['password']);
	$apikey=generateApiKeyCompany();
	$companyWorking=$user['companyWorking'];
	$status='Active';
	$isAgent=1;
	$check_d = $db->query("SELECT staffID FROM qb_companystaff WHERE emailAddress='".$email."'")->num_rows;
if($check_d == 0) {
	$db->query("INSERT INTO qb_companystaff(staffName,emailAddress,password,api_key,status,isAgent,companyWorking,profile_picture) VALUES
	('$full_name','$email','"._hash($password)."','$api_key','$status','$isAgent','$companyWorking','default_avatar.png')");
}
	
}


require('inc/top.php');
?>
<script src="<?php echo $domain;?>/vendor/jquery/dist/jquery.js"></script>
<script type="text/javascript">
function loadDashboard(id) {
  var agent = id;
 if(agent!= "") {
    $.get("<?php echo $domain;?>/dashboard/ajax/agentDashboard.php?agent="+agent, function(data) {
    $("#dashboard").html(data);
    //message.val("");
    });
    }
}
</script>
<section>
<div class="content-wrapper">
<div class="container-fluid">
<div class="col-md-12">

<div class="row">
<!-- START panel-->
<div class="col-md-3">
	<div class="panel panel-default">
		<div class="panel-heading">
<div class="panel-title">AGENTS </div>
</div>

<div data-height="300" data-scrollable="" class="list-group">

<?php 

				  $agents = $db->query("SELECT qb_companystaff.staffName,qb_companystaff.staffID FROM qb_companystaff WHERE companyWorking='".$user['companyWorking']."' AND isAgent=1");
			     if($agents->num_rows >= 1) {
                    while($details = $agents->fetch_array()) {
				

echo '<a onclick="loadDashboard('.$details['staffID'].')" class="list-group-item">
<div class="media-box">

<div class="media-box-body clearfix">

    <p class="mb-sm">
      <small>'.$details['staffName'].'</small>
    </p>
  </div>
</div>
</a>';

 
}
				  
}
				 
else { 
echo 'No Agents to Show'; } 
?>

</div>

		<!--<div class="panel-body">
		
			<form action="" method="post" role="form" enctype="multipart/form-data" id="settings">
				
				<?php
				  $agents = $db->query("SELECT qb_companystaff.staffName,qb_companystaff.staffID FROM qb_companystaff WHERE companyWorking='".$user['companyWorking']."' AND isAgent=1");
			     if($agents->num_rows >= 1) {
                    while($details = $agents->fetch_array()) {
				
					echo '<div class="form-group has-feedback"><label for="profile_picture"><a onclick="loadDashboard('.$details['staffID'].')">'.$details['staffName'].'</a></label></div>';
					
					}
					
				 }
				 else { 
						echo 'No Agents to Show'; 
				 
				 }
				 
					?>
				
				
			
			</form>
		</div>-->
	</div>
</div>
<!-- END panel-->
<!-- START panel-->
<div id="dashboard">
<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">RESOLVED</div>
		<div class="panel-body">
		<div class="col-md-12">

<div class="row">
<div class="col-md-4">
			10
	</div>
	<div class="col-md-4">
		20
	</div>
	<div class="col-md-4">
			30
	</div>
			
			</div>
			</div>
		</div>
		
	</div>
	
	<div class="panel panel-default">
		<div class="panel-heading">UNRESOLVED</div>
		<div class="panel-body">
			<div class="col-md-12">

<div class="row">
<div class="col-md-4">
			10
	</div>
	<div class="col-md-4">
		20
	</div>
	<div class="col-md-4">
			30
	</div>
			
			</div>
			</div>
		</div>
		
	</div>
</div>
</div>
<div class="col-md-3">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-plus"> Create Agent</i></div>
	
		<div class="panel-body">
			<?php if(isset($_COOKIE['updatedSettings'])) { ?> <div class="alert alert-success bg-success-light"> <i class="fa fa-check"></i> &nbsp <?php echo $lang['Updated_Account_Settings']?> </div> <?php } ?>
			<form action="" method="post" role="form" enctype="multipart/form-data" id="settings">
			
				<div class="form-group has-feedback">
					
					<input type="text" name="full_name" class="form-control" placeholder="Agent Name" value="<?php echo $user['full_name']?>" autocomplete="off" >
				
				</div>
				<div class="form-group has-feedback">
					
					<input type="email" name="email" class="form-control" placeholder="Email Address" value="<?php echo $user['email']?>" autocomplete="off" required>
				</div>
				<div class="form-group has-feedback">
					
					<input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off">
				</div>
				<div class="form-group has-feedback">
					
					<input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password" autocomplete="off">
				</div>
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo $lang['Save']?>">
			</form>
		</div>
	</div>
</div>
<!-- END panel-->
<!-- START panel
<div class="col-md-4">
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo $lang['Privacy']?></div>
		<div class="panel-body">
			<form action="" method="post">
				<div class="form-group">
					<label><?php echo $lang['Private_Profile']?></label>
					<div class="checkbox c-checkbox">
						<label>
							<input type="checkbox" name="private_profile" <?php echo $private_profile?> value="">
							<span class="fa fa-check"></span></label> <?php echo $lang['Check_To_Enable']?>
						</div>
						<p class="text-muted" style="display:inline;"><?php echo $lang['Private_Profile_Explanation']?></p>
					</div>
					<input type="submit" name="privacy_save" class="btn btn-danger" value="<?php echo $lang['Save']?>">
				</form>
			</div>
		</div>
	</div>
	<!-- END panel-->
</div>

</div>
</div>
</div>
</section>
<?php
require('inc/bottom.php'); 
?>
