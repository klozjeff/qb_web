<?php
session_set_cookie_params(172800);
session_start();
require('core/config/config.php');
require('core/config/config-theme.php');
require('core/config/config-lang.php');
require('core/system.php');

$db->set_charset('utf8');

if(isset($_GET['mobileapp'])) {
   $_SESSION['mobileapp'] = true;
} 

if(isset($_SESSION['mobileapp'])) {
   $mobileapp = true;
} else {
   $mobileapp = false;
}

$geo = geoinfo();

if(isset($_POST['register'])) {
	
    $contactName = $_POST['contactName'];
    $emailAddress =  $_POST['emailAddress'];
    $companyName =  $_POST['companyName'];
    $sector =  $_POST['sector'];
    $staffNumber =  $_POST['staffNumber'];
	$pass =  $_POST['password'];
	$companyUrl = $_POST['companyUrl'];
	$country = $_POST['country'];
	$phoneNo =  $_POST['phoneNo'];

// Geolocation
$country = $geo['geoplugin_countryName'];
$city = '';
$ip = $_SERVER['REMOTE_ADDR'];
$latitude = $geo['geoplugin_latitude'];
$longitude = $geo['geoplugin_longitude'];

// Check duplicate
$check_d = $db->query("SELECT companyID FROM qb_company WHERE companyName='".$contactName."'")->num_rows;
if($check_d == 0) {
$db->query("INSERT INTO qb_company(logo_picture,contactName,adminEmail,companyName,sector,staff_number,accountURL,country,telephone) 
	VALUES ('default_avatar.png','$contactName','$emailAddress','$companyName','$sector','$staffNumber','$companyUrl','$country','$phoneNo')");
$last_id = $db->insert_id;
if($last_id!='')
{
$apikey=generateApiKeyCompany();
$status='Active';
$isAdmin=1;	
$db->query("INSERT INTO qb_companystaff(profile_picture,staffName,emailAddress,password,api_key,status,isAdmin,companyWorking) VALUES ('default_avatar.png','$contactName','$emailAddress','"._hash($pass)."','$apikey','$status','$isAdmin','$last_id')");
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
mail($emailAddress,$site_name.' - '.sprintf($clean,$contactName), '<a href="'.$domain.'/app/chat/'.$user1.'">'.sprintf($lang['messaged_you'],$contactName).'</a>',$headers,'-f info@'.$domain);

}
setcookie('justRegistered', 'true', time()+6);
setcookie('mm-email',$emailAddress,time()+60*60*24*30,'/');
header('Location: ../index.html');
exit;
}
}

?>
