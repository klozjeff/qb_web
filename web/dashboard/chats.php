<?php
session_set_cookie_params(172800);
session_start();
require('core/config/config.php');
require('core/config/config-theme.php');
require('core/config/config-lang.php');
require("core/system.php");

$page['name'] = $lang['Chats'];
$menu['chat'] = 'active';

$conversations = $db->query("SELECT * FROM qb_tickets WHERE (receiver_id='".$user['companyWorking']."' OR sender_id='".$user['companyWorking']."')  ORDER BY last_activity DESC");

if(isset($_POST['delete_conversation'])) {
$delete = $_POST['delete'];
foreach($delete as $k=>$v) {
$db->query("DELETE FROM qb_tickets WHERE id='".$v."'");
}
header('Location: '.$domain.'/dashboard/chats');
exit;
}

if(isset($_POST['assign_agent'])) 
{ 

$ticket_id = $_POST['ticket_id'];
$agent_id = $_POST['agent_id'];

$db->query("UPDATE qb_tickets SET assignedTo='".$agent_id."' WHERE id='".$ticket_id."'");

}

$users = $db->query("SELECT id FROM qb_users WHERE companyFollowing='".$user['companyWorking']."'")->num_rows;
$purchases = $db->query("SELECT * FROM qb_feeds WHERE sender_id='".$user['companyWorking']."'")->num_rows;
$chats = $db->query("SELECT * FROM qb_tickets WHERE receiver_id='".$user['companyWorking']."'")->num_rows;

require('inc/top.php');
?>

<section>
<div class="content-wrapper">

<div class="container">
<div class="row">

<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-purple">
<div class="row row-table">
<div class="col-xs-4 text-center bg-purple-dark pv-lg">
<em class="icon-credit-card fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $purchases?></div>
<div class="text-uppercase">Feeds</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-primary">
<div class="row row-table">
<div class="col-xs-4 text-center bg-primary-dark pv-lg">
<em class="icon-user fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $users?></div>
<div class="text-uppercase">Loggers</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-12">
<div class="panel widget bg-green">
<div class="row row-table">
<div class="col-xs-4 text-center bg-green-dark pv-lg">
<em class="icon-bubbles fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0"><?php echo $chats?></div>
<div class="text-uppercase">Chats</div>
</div>
</div>
</div>
</div>

<div class="col-lg-3 col-sm-6">
<div class="panel widget bg-purple">
<div class="row row-table">
<div class="col-xs-4 text-center bg-purple-dark pv-lg">
<em class="icon-credit-card fa-3x"></em>
</div>
<div class="col-xs-8 pv-lg">
<div class="h2 mt0">3546</div>
<div class="text-uppercase">Payshop</div>
</div>
</div>
</div>
</div>
</div>
<div class="table-grid table-grid-desktop">
<div class="col">
<div class="clearfix mb">
<div class="btn-group pull-left">
</div>
</div>
<?php if($conversations->num_rows >= 1) { ?>
<form action="" method="post">
<div class="row">
<div class="col-md-5">
<!--<button type="submit" name="new" class="btn btn-default"> <i class="fa fa-pencil fa-fw"></i> New Message </button>-->
<button type="submit" name="delete_conversation" class="btn btn-danger"> <i class="fa fa-trash fa-fw"></i> <?php echo $lang['Delete']?> </button>

</div>
</div>
<br>
<div class="panel panel-default">
<div class="panel-body">

<table class="table table-responsive mb-mails">
	   <thead>
	  
	   <th style="text-align:left;">C.No</th>
	        <th style="text-align:left;">Name</th>
			 <th style="text-align:left;">Ticket Category</th>
                    <th style="text-align:left;">Chats</th>
					  <th style="text-align:left;"> Posted </th>
                    <th style="text-align:left;"> Actions </th>
                    <th style="text-align:center;">  </th>
                </thead>
		<tbody>
			<?php 
			$t=0;
				 while($convers = $conversations->fetch_array()) {
				if($convers['sender_id']!= $user['companyWorking']) { $other_user = $db->query("SELECT qb_users.id AS id,qb_users.unique_id AS full_name,qb_users.telephone AS my_name,qb_users.profile_picture FROM qb_users WHERE id='".$convers['sender_id']."'")->fetch_array(); } 
				//if($convers['receiver_id']= $user['companyWorking']) { $other_user = $db->query("SELECT qb_company.companyName as full_name,qb_company.companyID AS id FROM qb_company WHERE companyID='".$convers['receiver_id']."'")->fetch_array(); } 
				
				$t+=1;
			?>
				<tr>
				
				<td>
	                <a href="chat/<?php echo $other_user['id']?>" style="text-decoration:none;">
							
							
							
							<div class="mb-mail-meta">
								<div class="pull-left">
									<div class="mb-mail-preview"><?php echo $convers['ticketID']?></div>
								</div>
								
							</div>

						</a>
             	</td>
					<td>
						<a href="chat/<?php echo $other_user['id']?>" style="text-decoration:none;">
							<img src="<?php echo getProfilePicture($domain,$other_user)?>" class="mb-mail-avatar pull-left">
							<div class="mb-mail-meta">
								<div class="pull-left">
									<div class="mb-mail-subject" style="color:#515253;"><?php echo $other_user['my_name']?></div>
								</div>
								
							</div>
						</a>
					</td>
					<td>
						<a href="order/<?php echo $details['id']?>" style="text-decoration:none;">
							
						
							<div class="mb-mail-meta">
							
								<div class="mb-mail-preview"><?php echo parseEmoticons($domain,trimMessage($convers['last_message'],100))?></div>
							
							
							</div>

						</a>
					</td>
					<td><?php echo time_ago($convers['last_activity'])?></td>
					
						<td>
						<?php if($convers['isClosed']=='0')
					{
						?>
						<a class="" href="<?php echo $domain?>/app/admin/edit-user.php?id=<?php echo $user['id']?>"> <i class="fa fa-reply"></i> </a> 
					<?php if($convers['assignedTo']=='0')
					{
						?>
						  <a data-toggle="modal" data-target="#requestWriter<?php echo $convers['id'];?>"> 
            <i class="fa fa-user"  title="Assign to Agent"></i>
            </a>
			<?php
					}
					
					?>
			
						<a class="" href="<?php echo $domain?>/dashboard/close-ticket.php?id=<?php echo $convers['id']?>"> <i class="fa fa-close"></i> </a>
						<?php
					}
					else
					{
					echo '<button class="btn btn-success"> <i class="fa fa-check-square-o fa-fw"></i> Ticket Resolved </button>';
	
						
					}
					?>
						</td>
					
					<td style="width:40px;">
	                <div class="checkbox c-checkbox">
	                   <label>
	                      <input name="delete[]" type="checkbox" value="<?php echo $convers['id']?>">
	                      <span class="fa fa-check"></span>
	                   </label>
	                </div>
					
					<!--Modal start-->
					<div class="modal fade in" style="margin-top:10%" id="requestWriter<?php echo $convers['id'];?>" tabindex="-6" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<form action="" method="post" role="form" enctype="multipart/form-data">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Assign Ticket to Agent</h4>
      </div>
      <div class="modal-body">
	  
	  	<div class="panel-body">
  <div class="form-group has-feedback">
					<label>Select Agent</label>
					 <select name="agent_id" class="form-control">
					 <option value="">Select Value</option>
							<?php
		$agent_select = $db->query("SELECT * FROM qb_companystaff WHERE companyWorking='".$user['companyWorking']."' AND isAgent='1' ORDER BY staffID DESC");

   foreach($agent_select as $special) { 
        
          echo '<option value="'.$special['staffID'].'">'.$special['staffName'].'</option>';
        
      } ?>
    </select>
				
				
				<input type="hidden" name="ticket_id" value="<?php echo $convers['id'];?>" >
				
				</div>
   
      </div>
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"></button>
        <button type="submit" name="assign_agent" class="btn btn-success">Assign Agent</button>
      </div>
    </div>
    <input type="hidden" id="giftValue" name="giftValue">
  </form>
  </div>
</div>
					<!--Modal end-->
             	</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	<!--<table class="table mb-mails">
		<tbody>
			<?php while($convers = $conversations->fetch_array()) {
				if($convers['sender_id']!= $user['companyWorking']) { $other_user = $db->query("SELECT qb_users.id AS id,qb_users.unique_id AS full_name FROM qb_users WHERE id='".$convers['sender_id']."'")->fetch_array(); } 
				if($convers['receiver_id'] != $user['companyWorking']) { $other_user = $db->query("SELECT qb_company.companyName as full_name,qb_company.companyID AS id FROM qb_company WHERE companyID='".$convers['receiver_id']."'")->fetch_array(); } 
				?>
				<tr>
				<td style="width:40px;">
	                <div class="checkbox c-checkbox">
	                   <label>
	                      <input name="delete[]" type="checkbox" value="<?php echo $convers['id']?>">
	                      <span class="fa fa-check"></span>
	                   </label>
	                </div>
             	</td>
					<td>
						<a href="chat/<?php echo $other_user['id']?>" style="text-decoration:none;">
							<img src="<?php echo getProfilePicture($domain,$other_user)?>" class="mb-mail-avatar pull-left">
							<div class="mb-mail-date pull-right"><?php echo time_ago($convers['last_activity'])?></div>
							<div class="mb-mail-meta">
								<div class="pull-left">
									<div class="mb-mail-subject" style="color:#515253;"><?php echo $other_user['full_name']?></div>
								</div>
								<div class="mb-mail-preview"><?php echo parseEmoticons($domain,trimMessage($convers['last_message'],100))?></div>
							</div>
						</a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>-->
	</div>
</div>
</form>
<?php } else { echo $lang['No_Chats_To_Show']; } ?>
</div>
</div>
</div>
</div>
</section>

<?php
require('inc/bottom.php'); 
?>
