<?php

class QbCompany
{
    private $con;

    function __construct()
    {
        require_once '../include/DbConnect.php';
        $db = new DbConnect();
        $this->con = $db->connect();
    }

	 public function createCompany($contactName,$emailAddress,$companyName,$sector,$staffNumber,$pass,$companyUrl,$country,$phoneNo){
        if (!$this->isCompanyExists($companyName)) {
            $apikey = $this->generateApiKeyCompany();
			$password = md5($pass);
           $stmt = $this->con->prepare("INSERT INTO qb_company(contactName,adminEmail,companyName,sector,staff_number,accountURL,country,telephone) VALUES (?,?,?,?,?,?,?,?)");
           $stmt->bind_param("ssssssss",$contactName,$emailAddress,$companyName,$sector,$staffNumber,$companyUrl,$country,$phoneNo);
			$result = $stmt->execute();
			$lastID=$stmt->insert_id;
            $stmt->close();
			$status='Active';
			$isAdmin=1;
            if ($result) {	
		   $stmt_admin = $this->con->prepare("INSERT INTO qb_companystaff(staffName,emailAddress,password,api_key,status,isAdmin,companyWorking) VALUES (?,?,?,?,?,?,?)");
           $stmt_admin->bind_param("sssssss",$contactName,$emailAddress,$password,$apikey,$status,$isAdmin,$lastID);
		   $stmt_admin->execute();
           $stmt_admin->close();
				
                return 0;
            } else {
                return 1;
            }
        } else {
            return 2;
        }
    }
	
	 public function loginCompany($username,$pass){
        $password = md5($pass);
        $stmt= $this->con->prepare("SELECT * FROM qb_companystaff WHERE emailAddress=? and password=?");
        $stmt->bind_param("ss",$username,$password);
        $stmt->execute();
        $stmt->store_result();
        $num_rows=$stmt->num_rows;
        $stmt->close();
        return $num_rows>0;
    }
	
	 public function getStaff($username){
        $stmt = $this->con->prepare("SELECT * FROM qb_companystaff WHERE emailAddress=?");
        $stmt->bind_param("s",$username);
        $stmt->execute();
        $staff = $stmt->get_result()->fetch_assoc();
        $stmt->close();
        return $staff;
    }
	
	//Method to check if the phone number already exist or not

    private function isCompanyExists($companyName) {
        $stmt = $this->con->prepare("SELECT companyID from qb_company WHERE companyName = ?");
        $stmt->bind_param("s", $companyName);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }
	
	 private function generateApiKeyCompany(){
        return md5(uniqid(rand(), true));
    }

    


    

   
}

?>