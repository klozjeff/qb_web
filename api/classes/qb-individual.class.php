<?php

class QbIndividual
{
    private $con;

    function __construct()
    {
        require_once '../include/DbConnect.php';
        $db = new DbConnect();
        $this->con = $db->connect();
    }

    //Method to register a new individual
    public function createIndividual($telephone,$unique_id){
        if (!$this->isPhoneExists($telephone)) {
            $password = md5($unique_id);
            $apikey = $this->generateApiKey();
            $stmt = $this->con->prepare("INSERT INTO qb_users(unique_id,telephone,password,api_key) values(?, ?, ?, ?)");
            $stmt->bind_param("ssss", $unique_id,$telephone, $password, $apikey);
            $result = $stmt->execute();
            $stmt->close();
            if ($result) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 2;
        }
    }

 //method to activate new individual via activation code

    public function activateIndividual($unique_id,$status){
        if ($this->isValidCode($unique_id,$status)) {
            $stmt = $this->con->prepare("UPDATE qb_users SET status=? WHERE unique_id=?");
            $stmt->bind_param("ss", $status,$unique_id);
            $result = $stmt->execute();
            $stmt->close();
            if ($result) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 2;
        }
    }


//Method to check if the phone number already exist or not

    private function isPhoneExists($telephone) {
        $stmt = $this->con->prepare("SELECT id from qb_users WHERE telephone = ?");
        $stmt->bind_param("s", $telephone);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }


    //check if the activation code exist or not

    public function isValidCode($unique_id,$status) {
        $stmt = $this->con->prepare("SELECT id from qb_users WHERE unique_id = ? AND status<>?");
        $stmt->bind_param("ss", $unique_id,$status);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

   

     //Method to generate a unique api key for the users
    private function generateApiKey(){
        return md5(uniqid(rand(), true));
    }



   
}

?>